package br.com.jhsgdev.backend.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.jhsgdev.backend.models.Todo;
import br.com.jhsgdev.backend.models.vo.TodoVo;
import br.com.jhsgdev.backend.repository.TodoRepository;
import br.com.jhsgdev.backend.services.exceptions.ObjectNotFoundException;

/**
 * TodoService
 */
@Service
public class TodoService {

  @Autowired
  private TodoRepository repository;

  public List<Todo> findAllDone(Boolean done) {
    return this.repository.findByDone(done);
  }

  public List<TodoVo> findAllTodoVO(Boolean filter) {
    if (filter != null) { //done
      return this.findAllDone(filter).stream().map(ent -> this.toTodoVo(ent)).collect(Collectors.toList());  
    } else {
      return this.repository.findAll().stream().map(ent -> this.toTodoVo(ent)).collect(Collectors.toList());   
    }
  }

  public TodoVo show(Long id){         
    Todo obj = this.findById(id);    
    return this.toTodoVo(obj);
  }

  public TodoVo create(TodoVo objVo){
    objVo.setId(null);
    objVo.setDone(false);
    Todo result = this.save(this.toTodo(objVo));
    return this.toTodoVo(result);
  }         

  public TodoVo update(Long id, TodoVo objVo){
    Todo entity = this.findById(id);
    objVo.setId(entity.getId());

    Todo result = this.save(this.toTodo(objVo));
    return this.toTodoVo(result);
  }         

  public void destroy(Long id){
    Todo entity = this.findById(id);
    this.repository.delete(entity);
  }
  
  public void destroyAllDone(){
    List<Todo> list = this.findAllDone(true);
    this.repository.deleteAll(list);
  }

  private Todo save(Todo entity) {
    if (entity.getTitle() == null || entity.getTitle() == "") {
      throw new ObjectNotFoundException("Favor informar o Todo!");
    }
    return this.repository.saveAndFlush(entity);
  }

  public Todo findById(Long id){         
    Optional<Todo> entity = this.repository.findById(id);
    return entity.orElseThrow(() -> new ObjectNotFoundException("O Todo de Id: " + id + ", não foi encontrado!"));
  }

  public Todo toTodo(TodoVo objVO) {
    Todo obj = new Todo();
    
    obj.setId(objVO.getId());
    obj.setTitle(objVO.getTitle());
    obj.setDone(objVO.isDone());
    
    return obj;
  }

  public TodoVo toTodoVo(Todo obj) {

      TodoVo objVO = new TodoVo();    
      objVO.setId(obj.getId());
      objVO.setTitle(obj.getTitle());
      objVO.setDone(obj.isDone());
      
      return objVO;
  }
  
}