package br.com.jhsgdev.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.jhsgdev.backend.models.Todo;

/**
 * TodoRepository
 */
public interface TodoRepository extends JpaRepository<Todo, Long>{

  @Transactional(readOnly = true)             
  List<Todo> findByDone(Boolean done);
  
}