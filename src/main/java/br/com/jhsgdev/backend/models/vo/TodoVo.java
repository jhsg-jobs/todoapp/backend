package br.com.jhsgdev.backend.models.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TodoVo
 */
@NoArgsConstructor
@AllArgsConstructor
public @Data class TodoVo implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long id;  
  private String title;
  private boolean done;
  
}