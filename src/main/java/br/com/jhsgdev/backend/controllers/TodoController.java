package br.com.jhsgdev.backend.controllers;

import java.util.List;

import br.com.jhsgdev.backend.models.vo.TodoVo;
import br.com.jhsgdev.backend.services.TodoService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * TodoController
 */
@RestController
@RequestMapping(value = "/api/todos")
@CrossOrigin(origins = {"https://jhsg-todo-app.netlify.com/", "http://localhost:4200"})
public class TodoController {

  @Autowired
  private TodoService service;

  @GetMapping
  public List<TodoVo> all(@RequestParam(value="filter", defaultValue = "") Boolean filter) {
    System.out.println(filter);
    List<TodoVo> list = this.service.findAllTodoVO(filter);
    return list;
  }

  @GetMapping("/{id}")
  public TodoVo show(@PathVariable Long id) {
    return this.service.show(id);
  }

  @PostMapping
  public TodoVo create(@RequestBody TodoVo objVo) {
    return this.service.create(objVo);
  }
  
  @PutMapping("/{id}")
  public TodoVo update(@PathVariable Long id, @RequestBody TodoVo objVo) {    
    return this.service.update(id, objVo);
  }  
  
  @DeleteMapping("/{id}")
  public ResponseEntity<?> destroy(@PathVariable Long id) {
    this.service.destroy(id);
    return ResponseEntity.noContent().build();
  }

  @DeleteMapping("/done")
  public ResponseEntity<?> destroyAllDone() {
    this.service.destroyAllDone();
    return ResponseEntity.noContent().build();
  }

}