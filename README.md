# Backend TODO List

Para poder rodar o projeto localmente rodar os comandos dentro da pasta do projeto: 

 - ./mvnw spring-boot:run

Link de demonstração [Api Todo](https://jhsg-todo.herokuapp.com/api/todos).

Tendo os seguintes endpoints:

 - GET: https://jhsg-todo.herokuapp.com/api/todos?filter=false, retorna uma lista de Todos, sendo o paramentro filter opcional de ser informado
 - GET: https://jhsg-todo.herokuapp.com/api/todos/1, busca um todo pelo id
 - POST: https://jhsg-todo.herokuapp.com/api/todos, cria um novo Todo: enviar no corpo o seguint json: {"title" : "Flutter"}
 - PUT: https://jhsg-todo.herokuapp.com/api/todos/1 , busca e atualiza um todo pelo id espera um json {"title" : "Flutter"} no corpo da request
 - DELETE: https://jhsg-todo.herokuapp.com/api/todos/1, busca um todo pelo id e o apaga
 - DELETE: https://jhsg-todo.herokuapp.com/api/todos/done, apaga todos os todos concluidos  
 